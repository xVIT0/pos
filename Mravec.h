//
// Created by vitko on 04/01/2021.
//

#ifndef SEMESTRALKA_MRAVEC_H
#define SEMESTRALKA_MRAVEC_H

#include <stdbool.h>

//  pohyb :
//      hore    24     ↑
//      dole    25     ↓
//      vpravo  26     →
//      vlavo   27     ←



struct Mravec{
    int id;
    int x,y;
    int smer;
    bool nazive;
};

void setXY(struct Mravec* paMravec, int maxX, int maxY);

char* getSmer(struct Mravec* paMravec);

char zmenSmer(struct Mravec* paMravec, bool logikaMravcov, char farba);

#endif //SEMESTRALKA_MRAVEC_H
