
#include "Klient.h"

void write_file(struct Svet *svet ){
    FILE *fp;
    char filename[256];
    printf("Zadaj nazov suboru: \n");
    scanf("%s", filename);
    fp = fopen(filename, "w");
    if(fp == NULL)
    {
        printf("Nepodarilo sa vytvorit subor.\n");
        exit(1);
    }

    fprintf(fp, "%d ", svet->maxX);
    fprintf(fp, "%d ", svet->maxY);
    fprintf(fp, "%d ", svet->maxMravcov);
    fprintf(fp, "%d ", svet->aktMravcov);
    fprintf(fp, "%d ", svet->logikaMravcov);
    fprintf(fp, "%d\n", svet->stretnutie);

    for (int i = 0; i < svet->maxX; i++) {
        for (int j = 0; j < svet->maxY; j++) {
            fprintf(fp, "%d ", svet->stvorce[i][j].x);
            fprintf(fp, "%d ", svet->stvorce[i][j].y);
            fprintf(fp, "%c ", svet->stvorce[i][j].farba);
            if (svet->stvorce[i][j].mravec != NULL) {
                fprintf(fp, "%d\n", svet->stvorce[i][j].mravec->smer);
            } else {
                fprintf(fp,"0\n");
            }
        }
    }
    printf("Uspesne zapisane do suboru %s\n", filename);
    fclose(fp);
}

bool read_file(struct Svet *svet, bool svetPripraveny){
    znicSvet(svet, svetPripraveny);

    FILE *fp;
    char filename[256];
    int logika;
    int pokus = 3;
    printf("Zadaj nazov suboru: \n");
    scanf("%s", filename);
    fp = fopen(filename, "r");
    while(fp == NULL)
    {
        if (pokus == 0) {
            printf("Zadal si prilis vela neplatnych pokusov - navrat do menu\n");
            return false;
        }
        printf("Subor so zadanym menom sa nenasiel.\n");
        printf("Zadaj nazov suboru (mas este %d pokus/y) \n",pokus);
        scanf( "%s", filename);
        fp = fopen(filename, "r");
        pokus--;
    }
    if (fp != NULL) {
        fscanf(fp, "%d ", &svet->maxX);
        fscanf(fp, "%d ", &svet->maxY);
        fscanf(fp, "%d ", &svet->maxMravcov);
        fscanf(fp, "%d ", &svet->aktMravcov);
        fscanf(fp, "%d ", &logika);

        svet->logikaMravcov = (logika==0) ? true : false;
        fscanf(fp, "%d\n", &svet->stretnutie);
        svet->stvorce = malloc(sizeof(struct Stvorec*) * svet->maxX);
        for (int i = 0; i < svet->maxX; i++) {
            svet->stvorce[i] = malloc(sizeof(struct Stvorec) * svet->maxY);
        }
        svet->mravce[svet->maxMravcov];
        svet->mravce = malloc(sizeof(struct Mravec*) * svet->maxMravcov);

        int mravec;
        int iteratorMravcov = 0;
        for (int i = 0; i < svet->maxX; i++) {
            for (int j = 0; j < svet->maxY ; j++) {
                fscanf(fp, "%d ", &svet->stvorce[i][j].x);
                fscanf(fp, "%d ", &svet->stvorce[i][j].y);
                fscanf(fp, "%c ", &svet->stvorce[i][j].farba);
                svet->stvorce[i][j].pocetMravcov = 0;
                fscanf(fp, "%d\n", &mravec);
                if (mravec != 0) {
                    struct Mravec *novymravec = malloc(sizeof(struct Mravec));
                    novymravec->id = iteratorMravcov + 1;
                    novymravec->x = svet->stvorce[i][j].x;
                    novymravec->y = svet->stvorce[i][j].y;
                    novymravec->smer = mravec;
                    novymravec->nazive = true;
                    svet->mravce[iteratorMravcov] = novymravec;
                    svet->stvorce[i][j].mravec = novymravec;
                    svet->stvorce[i][j].pocetMravcov++;
                    iteratorMravcov++;
                } else {
                    svet->stvorce[i][j].mravec = NULL;
                }

            }
        }
        printf("Uspesne nacitany subor %s\n", filename);
        fclose(fp);
        return true;
    }
    return false;
}

int connectK(struct hostent* server, int host) {
    int sockfd;
    struct sockaddr_in serv_addr;
    bzero((char*)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(
            (char*)server->h_addr,
            (char*)&serv_addr.sin_addr.s_addr,
            server->h_length
    );
    serv_addr.sin_port = htons(host);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("Chyba pri vytvaraní socketu");
    }

    printf("Idem pripajat na server\n");

    if(connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("Chyba pri pripajani sa na socket");
    }

    return sockfd;
}

void write_Server(struct Svet *svet, struct hostent* server, int host) {
    int sockfd = connectK(server, host);
    char buffer[256];
    int *temp;
    read(sockfd,buffer,256); //citanie spravy zo servera po pripojeni
    printf("%s\n" ,buffer);

    char* msg = "Klient pripojeny  - pokus o zapis";
    write(sockfd, msg, 256); //odoslanie spravy o pripojeni
    bzero(buffer,256); //nulovanie buffra
    int zapis = 2;
    write(sockfd, &zapis, 256);
    printf("Zadaj nazov suboru: ");
    scanf( "%s", buffer);
    write(sockfd, buffer, 256);

    temp = malloc(sizeof(int)*6);

    temp[0] = svet->maxX;
    temp[1] = svet->maxY;
    temp[2] = svet->maxMravcov;
    temp[3] = svet->aktMravcov;
    temp[4] = svet->logikaMravcov;
    temp[5] = svet->stretnutie;

    write(sockfd, temp, sizeof(int)*6);
    free(temp);
    temp = malloc(sizeof(int)*3);

    char farba;
    for (int i = 0; i < svet->maxX; i++) {
        for (int j = 0; j < svet->maxY ; j++) {
            temp[0] = svet->stvorce[i][j].x;
            temp[1] = svet->stvorce[i][j].y;
            farba = svet->stvorce[i][j].farba;
            if (svet->stvorce[i][j].mravec == NULL) {
                temp[2] = 0;
            } else {
                temp[2] = svet->stvorce[i][j].mravec->smer;
            }

            write(sockfd, temp, sizeof(int)*3);
            write(sockfd, &farba, sizeof(char));
        }
    }

    char* msg2 = "Klient: Subor odoslany";
    write(sockfd, msg2, 256);
    bzero(buffer,256);
    read(sockfd,buffer,256);
    printf("%s\n" ,buffer);
    free(temp);
    close(sockfd);
}

bool read_Server(struct Svet *svet, struct hostent* server, int host, bool svetPripraveny) {
    znicSvet(svet, svetPripraveny);
    int sockfd = connectK(server, host);
    char buffer[256];
    int *temp;
    read(sockfd,buffer,256); //citanie spravy zo servera po pripojeni
    printf("%s\n" ,buffer);
    int chyba;
    char* msg = "Klient pripojeny - pokus o citanie";
    write(sockfd, msg, 256); //odoslanie spravy o pripojeni
    bzero(buffer,256); //nulovanie buffra
    int citanie = 1;
    write(sockfd, &citanie, 256);
    printf("Zadaj nazov suboru: ");
    scanf( "%s", buffer);
    write(sockfd, buffer, 256);
    //subor nespravny
    read(sockfd,&chyba,sizeof(int));
    while (chyba < 4) {
        printf("Nespravny nazov suboru - mas este %d pokusov \n", 4-chyba);
        printf("Zadaj nazov suboru: \n");
        scanf("%s", buffer);
        write(sockfd, buffer, 256);
        read(sockfd, &chyba, sizeof(int));
        if (chyba == 4) {
            printf("Pokusy vycerpane - navrat do menu\n");
            close(sockfd);
            return false;
        }
    }

    temp = malloc(sizeof(int)*6);
    read(sockfd,temp,sizeof(int)*6);

    svet->maxX = temp[0];
    svet->maxY = temp[1];
    svet->maxMravcov = temp[2];
    svet->aktMravcov = temp[3];
    svet->logikaMravcov = temp[4];
    svet->stretnutie = temp[5];

    svet->stvorce = malloc(sizeof(struct Stvorec*) * svet->maxX);
    for (int i = 0; i < svet->maxX; i++) {
        svet->stvorce[i] = malloc(sizeof(struct Stvorec) * svet->maxY);
    }
    svet->mravce[svet->maxMravcov];
    svet->mravce = malloc(sizeof(struct Mravec*) * svet->maxMravcov);

    free(temp);

char farba;
int iteratorMravcov = 0;
temp = malloc(sizeof(int)*3);

    for (int i = 0; i < svet->maxX; i++) {
        for (int j = 0; j < svet->maxY ; j++) {
            read(sockfd, temp, sizeof(int)*3);
            read(sockfd, &farba, sizeof(char));

            svet->stvorce[i][j].x = temp[0];
            svet->stvorce[i][j].y = temp[1];
            svet->stvorce[i][j].farba = farba;
            if (temp[2] == 0) {
                svet->stvorce[i][j].mravec = NULL;
            } else {
                struct Mravec *novymravec = malloc(sizeof(struct Mravec));
                novymravec->id = iteratorMravcov + 1;
                novymravec->x = svet->stvorce[i][j].x;
                novymravec->y = svet->stvorce[i][j].y;
                novymravec->smer = temp[2];
                novymravec->nazive = true;
                svet->mravce[iteratorMravcov] = novymravec;
                svet->stvorce[i][j].mravec = novymravec;
                svet->stvorce[i][j].pocetMravcov++;

                iteratorMravcov++;
            }
        }
    }
    char* msg2 = "Klient: Subor prijaty";
    write(sockfd, msg2, 256);
    bzero(buffer,256);
    read(sockfd,buffer,256);
    printf("%s\n" ,buffer);
    free(temp);
    close(sockfd);
    return true;
}
