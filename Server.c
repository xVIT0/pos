#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

void zapis_server(int sockfd) {
    char buff[256];
    int *temp;
    read(sockfd,buff,256);
    FILE *fp;
    fp = fopen(buff, "w");
    if(fp == NULL)
    {
        printf("Nepodarilo sa vytvorit subor\n");
        exit(1);
    }
    printf("Zapis do suboru: %s\n ",buff);

    temp = malloc(sizeof(int)*6);
    read(sockfd, temp, sizeof(int)*6);
    fprintf(fp, "%d ", temp[0]);
    fprintf(fp, "%d ", temp[1]);
    fprintf(fp, "%d ", temp[2]);
    fprintf(fp, "%d ", temp[3]);
    fprintf(fp, "%d ", temp[4]);
    fprintf(fp, "%d\n", temp[5]);

    int maxX = temp[0];
    int maxY = temp[1];
    char farba;
    free(temp);
    temp = malloc(sizeof(int)*3);
    for (int i = 0; i < maxX; ++i) {
        for (int j = 0; j < maxY; ++j) {
            read(sockfd, temp, sizeof(int)*3);
            read(sockfd, &farba, sizeof(char));
            fprintf(fp, "%d ", temp[0]);
            fprintf(fp, "%d ", temp[1]);
            fprintf(fp, "%c ", farba);
            fprintf(fp, "%d\n", temp[2]);
        }
    }
    read(sockfd,buff,256);
    printf("%s\n",buff);
    char* msg2 = "Server: Subor prijaty";
    write(sockfd,msg2,256);
    fclose(fp);
    //close(sockfd);
}

void citanie_server(int sockfd) {
    char buffer[256];
    int *temp;
    read(sockfd,buffer,256);
    FILE *fp;
    fp = fopen(buffer, "r");
    int chyba = 4;
    if (fp == NULL) {
        chyba = 1;
    }
    write(sockfd, &chyba, sizeof(int));
    while (fp == NULL)
    {
        printf("Subor so zadanym menom sa nenasiel. Klient ma este %d pokus/y\n", 4-chyba);
        read(sockfd,buffer,256);
        fp = fopen(buffer, "r");
        if (fp) {
            printf("Otvoreny subor\n");
            chyba = 4;
            write(sockfd, &chyba, sizeof(int));
        }
        if (chyba == 3) {
            chyba++;
            write(sockfd, &chyba, sizeof(int));
            break;
        }
        chyba++;
        write(sockfd, &chyba, sizeof(int));
    }

    if (fp != NULL) {
        printf("Otvoreny subor: %s\n",buffer);
        temp = malloc(sizeof(int)*6);

        fscanf(fp, "%d ", &temp[0]);
        fscanf(fp, "%d ", &temp[1]);
        fscanf(fp, "%d ", &temp[2]);
        fscanf(fp, "%d ", &temp[3]);
        fscanf(fp, "%d ", &temp[4]);
        fscanf(fp, "%d\n", &temp[5]);

        int maxX = temp[0];
        int maxY = temp[1];

        write(sockfd, temp, sizeof(int)*6);
        free(temp);
        temp = malloc(sizeof(int)*3);

        char farba;

        for (int i = 0; i < maxX; i++) {
            for (int j = 0; j < maxY ; j++) {
                fscanf(fp, "%d ", &temp[0]);
                fscanf(fp, "%d ", &temp[1]);
                fscanf(fp, "%c ", &farba);
                fscanf(fp, "%d\n", &temp[2]);
                write(sockfd, temp, sizeof(int)*3);
                write(sockfd, &farba, sizeof(char));

            }
        }
        read(sockfd,buffer,256);
        printf("%s\n",buffer);
        char* msg2 = "Server: Subor odoslany";
        write(sockfd,msg2,256);
        fclose(fp);
        free(temp);
    }
}

int main(int argc, char *argv[])
{
    setbuf(stdout,0);
    int sockfd, newsockfd;
    socklen_t cli_len;
    struct sockaddr_in serv_addr, cli_addr;
    char buffer[256];

    if (argc < 2)
    {
        fprintf(stderr,"Program %s nema zadany port v argumentoch\n", argv[0]);
        return 1;
    }

    bzero((char*)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(atoi(argv[1]));

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("Chyba pri vytvaraní socketu");
        return 1;
    }

    if (bind(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("Chyba pri bindingu adresy socketu");
        return 2;
    }
    printf("Server je spusteny\n");

    bool koniec = false;
    while (!koniec) {
        listen(sockfd, 5);
        cli_len = sizeof(cli_addr);

        newsockfd = accept(sockfd, (struct sockaddr*)&cli_addr, &cli_len);
        if (newsockfd < 0)
        {
            perror("Chyba pri prijati spojenia");
            return 3;
        }

        int typ;

        char* msg = "Server: Si pripojeny na server";
        write(newsockfd, msg, 256); //odoslanie spravy
        read(newsockfd,buffer,256); //citanie spravy z klienta
        printf( "%s\n", buffer);
        read(newsockfd,&typ,256); //citanie typu

        switch (typ) {
            case 1:
                citanie_server(newsockfd);
                break;
            case 2:
                zapis_server(newsockfd);
                break;
            default:
                break;
        }

        printf("Chces vypnut server ? \n"
               "1: Ano\n"
               "2: Nie\n");
        int rozhodnutie = 0;
        scanf("%d", &rozhodnutie);
        if (rozhodnutie == 1) {
            koniec = true;
        }
    }

    if (sockfd >= 0)
    {
        close(sockfd);
    }

    if (newsockfd >= 0)
    {
        close(newsockfd);
    }

    return 0;
}
