//
// Created by vitko on 04/01/2021.
//

#include "Mravec.h"

void setXY(struct Mravec* mravec, int maxX, int maxY) {
    switch (mravec->smer) {
        case 24 :
            mravec->y -= 1;
            break;
        case 25 :
            mravec->y += 1;
            break;
        case 26 :
            mravec->x += 1;
            break;
        case 27 :
            mravec->x -= 1;
            break;
    }
    //ak sa mravcove suradnice dostanu mimo plochy
    if(mravec->x < 0) {
        mravec->x = maxX - 1;
    } else if(mravec->x >= maxX) {
        mravec->x = 0;
    } else if(mravec->y < 0) {
        mravec->y = maxY - 1;
    } else if(mravec->y >= maxY) {
        mravec->y = 0;
    }
}


char* getSmer(struct Mravec* paMravec) {
    char* smer;
    switch (paMravec->smer) {
        case 24:
            smer="↑";
            break;
        case 25:
            smer="↓";
            break;
        case 26:
            smer="→";
            break;
        case 27:
            smer="←";
            break;
    }
    return smer;
}
//zmeni smer mravca a vrati pismeno oznacujuce zmenenu farbu policka
char zmenSmer(struct Mravec* paMravec, bool logikaMravcov, char farba) {
    char zmenaFarby;
    if(farba == 'B') {
        zmenaFarby = 'C';
        switch (paMravec->smer) {
            case 24:
                paMravec->smer = (logikaMravcov) ? 26 : 27;
                break;
            case 25:
                paMravec->smer = (logikaMravcov) ? 27 : 26;
                break;
            case 26:
                paMravec->smer = (logikaMravcov) ? 25 : 24;
                break;
            case 27:
                paMravec->smer = (logikaMravcov) ? 24 : 25;
                break;
        }
    }else {
        zmenaFarby = 'B';
        switch (paMravec->smer) {
            case 24:
                paMravec->smer = (logikaMravcov) ? 27 : 26;
                break;
            case 25:
                paMravec->smer = (logikaMravcov) ? 26 : 27;
                break;
            case 26:
                paMravec->smer = (logikaMravcov) ? 24 : 25;
                break;
            case 27:
                paMravec->smer = (logikaMravcov) ? 25 : 24;
                break;
        }
    }
    return zmenaFarby;
}