//
// Created by vitko on 04/01/2021.
//

#include "Svet.h"

int getPocetCiernychPoli(struct Svet* svet) {
    int pocet = 0;
    for (int i = 0; i < svet->maxX; i++) {
        for (int j = 0; j < svet->maxY; ++j) {
            pocet++;
        }
    }
    return pocet;
}

void priradParametre(struct Svet* svet) {
    printf("Zadaj velkost sveta: \n");
    scanf("%d", &svet->maxX);
    while(getchar() !='\n') {
        continue;
    }
    scanf("%d", &svet->maxY);
        while(getchar() !='\n') {
            continue;
        }
    int volba= 0;
    while (true) {
        printf("Zadaj pocet mravcov: \n");
        scanf("%d",&volba);
        while(getchar() !='\n') {
            continue;
        }
        if(volba <= (svet->maxX * svet->maxY)) {
            svet->maxMravcov = volba;
            break;
        }
    }
    while (true) {
        printf("Zadaj logiku mravcov: \n"
               "0 - priama\n"
               "1 - inverzna\n");
        scanf("%d",&volba);
        if(volba >= 0 && volba <= 1) {
            svet->logikaMravcov = (volba == 0) ? true : false;
            break;
        } else
            printf("Zle zadana volba.\n");
    }
}

void vytvorSvet(struct Svet* svet) {
    svet->stvorce[svet->maxX][svet->maxY];
    svet->stvorce = malloc(sizeof(struct Stvorec*) * svet->maxX);
    for (int i = 0; i < svet->maxX; i++) {
        svet->stvorce[i] = malloc(sizeof(struct Stvorec) * svet->maxY);
    }
    svet->mravce[svet->maxMravcov];
    svet->mravce = malloc(sizeof(struct Mravec*) * svet->maxMravcov);

    for (int i = 0; i < svet->maxX; i++) {
        for (int j = 0; j < svet->maxY ; j++) {
            svet->stvorce[i][j].x = i;
            svet->stvorce[i][j].y = j;
            svet->stvorce[i][j].farba = 'B';
            svet->stvorce[i][j].pocetMravcov = 0;
            svet->stvorce[i][j].mravec = NULL;
        }
    }
    svet->aktMravcov = 0;
}

void pridajMravce(struct Svet* svet) {
    int x;
    int y;
    printf("%d\n", svet->maxMravcov);

    for (int i = 0; i < svet->maxMravcov; i++) {
            do {
                x = rand()%(svet->maxX);
                y = rand()%(svet->maxY);
            }while (svet->stvorce[x][y].mravec != NULL);

            struct Mravec *mravec = malloc(sizeof(struct Mravec));
            mravec->id = i + 1;
            mravec->x = x;
            mravec->y = y;
            mravec->smer = 24 + rand() %4;
            mravec->nazive = true;

            svet->stvorce[mravec->x][mravec->y].mravec = mravec;
            svet->stvorce[mravec->x][mravec->y].farba = 'C';
            svet->mravce[svet->aktMravcov] = mravec;
            svet->aktMravcov++;
    }
}

void ukazSvet(struct Svet* svet) {
    char* smer = NULL;
    printf("Pocet mravcov: %d z %d\n", svet->maxMravcov, svet->aktMravcov);
    for (int i = 0; i < svet->maxY; i++) {
        for (int j = 0; j < svet->maxX; j++) {
            if(svet->stvorce[j][i].farba == 'B') {
                if(svet->stvorce[j][i].mravec == NULL) {
                    printf("\033[40m  \033[m");
                } else {
                    if(svet->stvorce[j][i].mravec->nazive) {
                        smer = getSmer(svet->stvorce[j][i].mravec);
                        printf("\033[91;40m%s \033[m",smer);
                    } else {
                        printf("\033[40m  \033[m");
                    }

                }
            } else {
                if(svet->stvorce[j][i].mravec == NULL) {
                    printf("\033[107m  \033[m");
                } else {
                    if(svet->stvorce[j][i].mravec->nazive) {
                        smer = getSmer(svet->stvorce[j][i].mravec);
                        printf("\033[91;107m%s \033[m",smer);
                    } else {
                        printf("\033[107m  \033[m");
                    }
                }
            }
        }
        printf("\n");
    }
    printf("\n");
}

void znicSvet(struct Svet* svet, bool svetPripraveny) {
    if(svetPripraveny) {
        for (int i = 0; i < svet->maxX; i++) {
            free(svet->stvorce[i]);
        }
        free(svet->stvorce);
        for (int i = 0; i < svet->maxMravcov; i++) {
            free(svet->mravce[i]);
        }
        free(svet->mravce);
    }
}

void posunMravca(struct Svet* svet, struct Mravec* mravec) {
    int staraX = mravec->x;
    int staraY = mravec->y;
    int pocetMrtvych = 0;

    setXY(mravec, svet->maxX, svet->maxY);

    if((svet->stretnutie == 1 || svet->stretnutie == 2) &&
    (svet->stvorce[mravec->x][mravec->y].mravec != NULL && svet->stvorce[mravec->x][mravec->y].mravec->nazive)) {
        if(svet->stretnutie == 1) {
            mravec->nazive = false;
            pocetMrtvych++;
            if(svet->stvorce[mravec->x][mravec->y].mravec->nazive == true) {
                pocetMrtvych++;
                svet->stvorce[mravec->x][mravec->y].mravec->nazive  = false;
            }
        }
        if(svet->stretnutie == 2) {
            pocetMrtvych++;
            mravec->nazive = false;
        }
    }
    else {
        char farba = svet->stvorce[mravec->x][mravec->y].farba;
        svet->stvorce[mravec->x][mravec->y].farba = zmenSmer(mravec, svet->logikaMravcov, farba);
        svet->stvorce[mravec->x][mravec->y].mravec = mravec;
    }
    svet->stvorce[staraX][staraY].mravec = NULL;

    svet->aktMravcov -= pocetMrtvych;
}

void potriedMravce(struct Svet* svet, int pocet) {
    bool najdeny;
    struct Mravec* temp;
    for (int i = 0; i < pocet; i++) {
        najdeny = false;
        for (int j = 0; j < svet->maxMravcov; j++) {
            if(najdeny) {
                temp = svet->mravce[j-1];
                svet->mravce[j-1] = svet->mravce[j];
                svet->mravce[j] = temp;
            }
            if(!najdeny) {
                if(!svet->mravce[j]->nazive) {
                    najdeny = true;
                }
            }
        }
    }
}

void vygenerujCiernePolia(struct Svet* svet) {
    int pocetPoli = 0;
    int randX;
    int randY;
    int maxX;
    int maxY;
    while(true) {
        printf("Kolko ciernych poli chcete vygenerovat? \n");
        scanf("%d", &pocetPoli);
        if(pocetPoli <= getPocetCiernychPoli(svet)) {
            break;
        } else {
            printf("Prosim zadajte spravny pocel poli!");
        }
    }
    while(pocetPoli > 0) {
        printf("%d\n",pocetPoli);
        maxX = svet->maxX;
        maxY = svet->maxY;
        randX = rand()%(maxX);
        randY = rand()%(maxY);
        if (svet->stvorce[randX][randY].farba != 'C') {
            printf("nasiel som na %d %d\n", randX, randY);
            svet->stvorce[randX][randY].farba = 'C';
            pocetPoli--;
        }
        printf("zafarbil som.\n");
    }
}

void definujCiernePolia(struct Svet* svet) {
    int pocetPoli = 0;
    while (true) {
        printf("Kolko ciernych poli chcete definovat? \n");
        scanf("%d", &pocetPoli);
        if (pocetPoli <= getPocetCiernychPoli(svet)) {
            break;
        }
    }

    int x;
    int y;
    for (int i = 0; i < pocetPoli; i++) {
        ukazSvet(svet);
        while (true) {
            printf("Zadajte suradnicu pola (zaciatok indexu je 0). \n");
            scanf("%d %d", &x, &y);
            if (x <= (svet->maxX) && y <= (svet->maxY)) {
                if (svet->stvorce[x][y].farba == 'B') {
                    break;
                }
                else {
                    printf("Pole uz je cierne.\n");
                }
            } else {
                printf("Zle zadane suradnice.\n");
            }
        }
        svet->stvorce[x][y].farba = 'C';
        printf("Pole uspesne zafarbene.\n");
    }
    ukazSvet(svet);
}

void definujStretnutieMravcov(struct Svet* svet) {
    int volba = -1;
    while (true) {
        printf("Co sa ma stat, ak sa stretnu viacere mravce:\n"
               "1: Zánik všetkých mravcov, ktoré sa stretli\n"
               "2: Prežije iba jeden mravec\n"
               "3: polovica mravcov sa začne správať podľa doplnkovej logiky\n");
        scanf("%d", &volba);

        if (volba >= 1 && volba <= 3)
            break;

    }
    svet->stretnutie = volba;
    printf("Volba zaznamenana.\n");
}

