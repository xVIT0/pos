#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <netdb.h>
#include "Svet.h"
#include "Klient.h"

// cakanie na getchar link:: https://fresh2refresh.com/c-programming/c-file-handling/putchar-getchar-function-c/

struct SvetData {
    struct Svet* svet;
    int pocetKrokov;
    int *aktualnyKrok;

    volatile bool * koniec;
    volatile bool * modifikovane;
    volatile bool * ukoncenie;

    pthread_mutex_t * mutex;
    pthread_cond_t * posun;
    pthread_cond_t * cakaj;
};

struct UkoncenieData {
    int pocetKrokov;

    volatile bool * ukoncenie;
    pthread_cond_t * cakaj;
};

struct MravecData {
    struct Svet* svet;
    struct Mravec* mravec;
    int *aktualnyKrok;

    volatile bool * koniec;
    volatile bool * modifikovane;
    volatile bool * ukoncenie;
    pthread_mutex_t * mutex;
    pthread_cond_t * posun;
    pthread_cond_t * cakaj;
} ;

void * procecUkoncenia(void * param) {
    struct UkoncenieData * data = (struct UkoncenieData *) param;
    if(data->pocetKrokov == 0) {
        char c = 'a';
        while (c != 'q')
        {
            c = getchar();
            if(c == 'q') {
                printf("Simulacia je prerusena.\n");
                (*data->ukoncenie) = true;
                pthread_cond_signal(data->cakaj);
                break;
            }
        }
    }
    return NULL;
}

void * procesMravca(void* param) {
    //mravec caka kym pride na jeho rad sa posunut

    struct MravecData * data = (struct MravecData *) param;

    while (!(*data->koniec)) {
        pthread_mutex_lock(data->mutex);
        while((*data->modifikovane)) {
            pthread_cond_wait(data->posun, data->mutex);
            if(!data->mravec->nazive || (*data->koniec)) {
                if(!data->mravec->nazive && data->svet->stvorce[data->mravec->x][data->mravec->y].mravec != NULL) {
                    data->svet->stvorce[data->mravec->x][data->mravec->y].mravec = NULL;
                }
                int pocetMravcov = data->svet->aktMravcov;
                pthread_mutex_unlock(data->mutex);
                if (pocetMravcov > 0) {
                    pthread_cond_signal(data->posun);
                } else {
                    pthread_cond_signal(data->cakaj);
                }
                return NULL;
            }
        }
        posunMravca(data->svet, data->mravec);
        (*data->aktualnyKrok)++;
        (*data->modifikovane) = true;
        if(!data->mravec->nazive) {
            pthread_mutex_unlock(data->mutex);
            pthread_cond_signal(data->cakaj);
            return NULL;
        }
        pthread_mutex_unlock(data->mutex);
        pthread_cond_signal(data->cakaj);
    }
    return NULL;
}

void * procesSveta(void* param) {
    //svet vykresluje mapu po kazdom posune mravca

    struct SvetData * data = (struct SvetData *) param;

    if (data->pocetKrokov == 0) {
        while (!(*data->ukoncenie) && data->svet->aktMravcov > 0) {
            pthread_mutex_lock(data->mutex);

            while(!(*data->modifikovane) && !(*data->ukoncenie)) {
                pthread_cond_wait(data->cakaj, data->mutex);
            }

            ukazSvet(data->svet);

            (*data->modifikovane) = false;

            pthread_mutex_unlock(data->mutex);
            sleep(2);
            pthread_cond_signal(data->posun);
        }

        pthread_mutex_lock(data->mutex);
        (*data->koniec) = true;
        printf("Svet po vykonani %d krokov:\n", (*data->aktualnyKrok));
        ukazSvet(data->svet);
        pthread_mutex_unlock(data->mutex);
        pthread_cond_signal(data->posun);
    } else {
        while ((*data->aktualnyKrok) < data->pocetKrokov && data->svet->aktMravcov > 0) {
            pthread_mutex_lock(data->mutex);
            while(!(*data->modifikovane) && data->svet->aktMravcov > 0) {
                pthread_cond_wait(data->cakaj, data->mutex);
            }

            (*data->modifikovane) = false;

            pthread_mutex_unlock(data->mutex);
            pthread_cond_signal(data->posun);
        }
        pthread_mutex_lock(data->mutex);
        (*data->koniec) = true;
        printf("Svet po vykonani %d krokov:\n", (*data->aktualnyKrok));
        ukazSvet(data->svet);
        pthread_mutex_unlock(data->mutex);
        pthread_cond_signal(data->posun);
    }
    return NULL;
}

void simulaciaKrokovMravcov(int pocetKrokov, struct Svet* svet) {
    int pocetMravcovNaZaciatku=svet->aktMravcov;
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t posun;
    pthread_cond_t cakaj;
    pthread_cond_init(&posun, NULL);
    pthread_cond_init(&cakaj, NULL);

    bool  koniec = false;
    bool  modifikovane = false;
    bool  ukoncenie = false;
    int aktualnyKrok = 0;

    struct SvetData svetData = {svet, pocetKrokov, &aktualnyKrok, &koniec, &modifikovane, &ukoncenie, &mutex, &posun, &cakaj};
    struct UkoncenieData ukoncenieData = {pocetKrokov, &ukoncenie, &cakaj};

    struct MravecData mravecData[svet->aktMravcov];
    for (int i = 0; i < svet->aktMravcov; i++) {
        mravecData[i].svet = svet;
        mravecData[i].mravec = svet->mravce[i];
        mravecData[i].aktualnyKrok = &aktualnyKrok;
        mravecData[i].koniec = &koniec;
        mravecData[i].modifikovane = &modifikovane;
        mravecData[i].ukoncenie = &ukoncenie;
        mravecData[i].mutex = &mutex;
        mravecData[i].posun = &posun;
        mravecData[i].cakaj = &cakaj;
    }

    printf("\nSvet pred zacatim simulacie:\n");
    ukazSvet(svet);

    pthread_t vlaknoSveta;
    pthread_t vlaknoMravca[svet->aktMravcov];
    pthread_t vlaknoUkoncenia;

    pthread_create(&vlaknoSveta, NULL, &procesSveta, &svetData);
    for (int i = 0; i < svet->aktMravcov; i++) {
        pthread_create(&vlaknoMravca[i], NULL, &procesMravca, &mravecData[i]);
    }
    pthread_create(&vlaknoUkoncenia,NULL, &procecUkoncenia, &ukoncenieData);

    pthread_join(vlaknoSveta, NULL);
    for (int i = 0; i < svet->aktMravcov; i++) {
        pthread_join(vlaknoMravca[i], NULL);
    }

    pthread_join(vlaknoUkoncenia, NULL);

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&posun);
    pthread_cond_destroy(&cakaj);

    potriedMravce(svet, (pocetMravcovNaZaciatku - svet->aktMravcov));
}

int main(int argc, char* argv[]) {
    srand(time(0));
    struct Svet svet;
    bool svetPripraveny = false;
    int volba;
    bool ulozenySvet = false;
    while(true) {
        volba = 0;
        while(true) {
            setbuf(stdout, 0);
            printf("Zadaj volbu:\n"
                   "1: Vytvor svet\n"
                   "2: Uloz svet\n"
                   "3: Modifikuj svet\n"
                   "4: Simuluj kroky mravcov\n"
                   "5: Ukoncit program\n");
            scanf("%d", &volba);
            if (volba >= 1 && volba <= 5) {
                break;
            }
        }

        int volbaModifikacie = 0;
        int volbaSimulacie = 0;
        int volbaUkoncenia = 0;
        int volbaSpustenia = 0;
        int volbaUlozenia = 0;

        switch (volba) {
            case 1:
                system("clear");
                while(true) {
                    printf("Ako chcete vytvorit svet?\n"
                           "1: Vytvorit svet\n"
                           "2: Nacitat svet lokalne\n"
                           "3: Nacitat svet zo servera\n");

                    scanf("%d", &volbaSpustenia);
                    if(volbaSpustenia >= 1 && volbaSpustenia <= 3) {
                        break;
                    }
                }

                if(volbaSpustenia == 1) {
                    znicSvet(&svet, svetPripraveny);
                    priradParametre(&svet);
                    definujStretnutieMravcov(&svet);
                    vytvorSvet(&svet);
                    pridajMravce(&svet);
                    svetPripraveny = true;
                } else if(volbaSpustenia == 2) {
                    svetPripraveny = read_file(&svet, svetPripraveny);
                } else {
                    struct hostent* server;
                    if (argc < 3)
                    {
                        fprintf(stderr,"Program %s nemá zadany hostname alebo port v argumentoch\n", argv[0]);
                        return 1;
                    }

                    server = gethostbyname(argv[1]);
                    if (server == NULL)
                    {
                        fprintf(stderr, "Chyba - host so zadanym menom neexistuje\n");
                        return 2;
                    }

                    svetPripraveny = read_Server(&svet, server, atoi(argv[2]), svetPripraveny);
                }
                break;
            case 2:
                system("clear");
                if(!svetPripraveny) {
                    printf("Svet nie je vytvoreny!\n");
                } else {
                    while(true) {
                        printf("Ako chcete ulozit svet?\n"
                               "1: Ulozit svet lokalne\n"
                               "2: Ulozit svet na server\n");

                        scanf("%d", &volbaUlozenia);
                        if(volbaUlozenia >= 1 && volbaUlozenia <= 2) {
                            break;
                        }
                    }

                    if (volbaUlozenia == 1) {
                        write_file(&svet);
                    }else {
                        struct hostent* server;
                        if (argc < 3)
                        {
                            fprintf(stderr,"Program %s nemá zadany hostname alebo port v argumentoch\n", argv[0]);
                            return 1;
                        }

                        server = gethostbyname(argv[1]);
                        if (server == NULL)
                        {
                            fprintf(stderr, "Chyba - host so zadanym menom neexistuje\n");
                            return 2;
                        }
                        write_Server(&svet, server, atoi(argv[2]));
                    }

                    ulozenySvet = true;
                }
                break;
            case 3:
                system("clear");
                if(!svetPripraveny) {
                    printf("Svet nie je vytvoreny!\n");
                    break;
                } else {
                    while(true) {
                        printf("Ako chcete modifikovat svet?\n"
                               "1: generovanie ciernych poli\n"
                               "2: manualne zadanie ciernych poli\n"
                               "3: navrat\n");

                        scanf("%d", &volbaModifikacie);
                        if(volbaModifikacie >= 1 && volbaModifikacie <= 3) {
                            break;
                        }
                    }
                    switch (volbaModifikacie) {
                        case 1:
                            vygenerujCiernePolia(&svet);
                            break;
                        case 2:
                            definujCiernePolia(&svet);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 4:
                system("clear");
                if(!svetPripraveny) {
                    printf("Svet nie je vytvoreny!\n");
                    break;
                } else {
                    while(true){
                        printf("Ako chcete simulovat kroky?\n"
                               "1: Zobrazovanie krokov mravcov po jednom. (ukoncenie klavesou q)\n"
                               "2: Zobrazenie sveta po urcitom pocte krokov\n");
                        scanf("%d", &volbaSimulacie);
                        if(volbaSimulacie >= 1 && volbaSimulacie <= 2) {
                            break;
                        }
                    }
                    if(volbaSimulacie == 2) {
                        while (true) {
                            printf("Kolko krokov chcete simulovat?\n");
                            scanf("%d", &volbaSimulacie);
                            if (volbaSimulacie > 0) {
                                break;
                            }
                        }
                    } else {
                        volbaSimulacie = 0;
                    }
                    simulaciaKrokovMravcov(volbaSimulacie, &svet);
                }
                break;
            case 5:
                system("clear");
                if(!ulozenySvet) {
                    while (true) {
                        printf("Svet nie je ulozeny, chcete ukoncit program aj napriek tomu?\n"
                               "1: Ano\n"
                               "2: Nie\n");
                        scanf("%d", &volbaUkoncenia);
                        if (volbaUkoncenia >= 1 && volbaUkoncenia <= 2) {
                            break;
                        }
                    }
                    if (volbaUkoncenia == 2) {
                        volba = 0;
                    }
                } else {
                    break;
                }
            default:
                break;
        }
        if(volba == 5) {
            break;
        }
    }

    znicSvet(&svet, svetPripraveny);
    return 0;
}
