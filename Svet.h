//
// Created by vitko on 04/01/2021.
//

#ifndef SEMESTRALKA_SVET_H
#define SEMESTRALKA_SVET_H

#include "Mravec.h"
#include <stdio.h>
#include <stdlib.h>


struct Stvorec {
    char farba;
    int x,y, pocetMravcov;
    struct Mravec* mravec;
};

struct Svet {
    int maxX, maxY, maxMravcov, aktMravcov, stretnutie;
    struct Stvorec** stvorce;
    struct Mravec** mravce;
    bool logikaMravcov;
};

int getPocetCiernychPoli(struct Svet* svet);

void priradParametre(struct Svet* svet);

void vytvorSvet(struct Svet* svet);

void pridajMravce(struct Svet* svet);

void ukazSvet(struct Svet* svet);

void znicSvet(struct Svet* svet, bool znic);

void posunMravca(struct Svet* svet, struct Mravec* mravec);

void potriedMravce(struct Svet* svet, int pocet);

void vygenerujCiernePolia(struct Svet* svet);

void definujCiernePolia(struct Svet* svet);

void definujStretnutieMravcov(struct Svet* svet);

#endif //SEMESTRALKA_SVET_H