//
// Created by vitko on 07/01/2021.
//

#ifndef SEMESTRALKA_KLIENT_H
#define SEMESTRALKA_KLIENT_H
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "Svet.h"

void write_file(struct Svet *svet);
bool read_file(struct Svet *svet, bool svetPripraveny);
int connectK(struct hostent* server, int host);
void write_Server(struct Svet *svet, struct hostent* server, int host);
bool read_Server(struct Svet *svet, struct hostent* server, int host, bool svetPripraveny);

#endif //SEMESTRALKA_KLIENT_H
